.. jsre documentation master file, created by
   sphinx-quickstart on Sat Apr  4 14:31:53 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

jsre Regular Expression Module
==============================

..  toctree::
    :numbered:
    :maxdepth: 2

    README
    FAQ
    performance
    examples
    install
    API
    re_syntax
    debugging
    unicode
    history
    Licence

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

