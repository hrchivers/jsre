rmdir build /q/s
rmdir dist /q/s
rmdir jsre.egg-info /q/s
rmdir jsre\docs /q/s
cd _docs
call make.bat html
cd ..
xcopy _docs\_build\html jsre\docs\html\ /S
copy _docs\README.rst README.rst
python setup.py bdist_wheel
python setup.py sdist
python setup.py bdist_msi
pause