/* **********************************************************

    Jigsaw Virtual Machine (tvms) Test Utils
    
    Provides external functions used in only testing
    
    @author: Howard Chivers
    @version: 1.1
    
	Copyright (c) 2015, Howard Chivers
	All rights reserved.
    
************************************************************ */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "jsvm.h"
#include "jsvm_test.h"

int checkRe(VirtualMachine *tvm,Runtime * rtm,char * buffer,int start,int stop, int end,int count,int refResult[][3]) {
	int res,resStart,resEnd,resLast;
	
	rtm->bufferSpec.start = start;
	rtm->bufferSpec.stop  = stop;
	rtm->bufferSpec.end   = end;
	initRunTime(tvm,rtm,0);
	while (TRUE) { 
		res = findNextMatch(tvm,rtm,(uint8_t *)buffer);
		if (res) {
			printf("findMatch() index failed from %i,%i, result = %i\n",rtm->startIndex,rtm->anchor,res);
			return -1;
		}
		if (rtm->published) {
			resStart = rtm->groupMark[0].start;
			resEnd   = rtm->groupMark[0].end;
			if (rtm->published & 0x80000000) {
				resLast  = rtm->groupMark[MAX_MARKS-1].end;
			} else {
				resLast = -1;
			}
			printf("%.*s   (%i,%i)\n", resEnd - resStart + 1, buffer + resStart,resStart,resEnd);
			
			if (resStart != refResult[count][0] || resEnd != refResult[count][1] || resLast != refResult[count][2]) {
				printf("findNextMatch() test %i incorrect result : %i,%i,%i (correct result = %i,%i,%i )\n",count,resStart,resEnd,resLast,refResult[count][0], refResult[count][1],refResult[count][2]);
				return -1;
			} else {
				count++;
			}
		} else {
			return count;
		}
	}

}

int countChain(ThreadState * start) {
	/*
	count number of items in thread chain
	*/
	int res=0;
	ThreadState * tst;
	if (start==NULL) return 0;
	tst = start;
	while (tst!=NULL) {
		res++;
		tst = tst->next;
	}
	return res;
}

int writeTestProgram(VirtualMachine * tvm, int instr[][4],int len) {
	int i,posn,res;
	ByteCode code[256];
	posn = tvm->programLength;
	for (i=0;i<len;i++) { //prod code would check len!
		code[i].instruction = instr[i][0];
		if (instr[i][3])
			code[i].instruction |= FLAG_FINAL;
		code[i].index       = instr[i][1];
		code[i].address     = instr[i][2];
	}
	res = newProgram(tvm,code,len);
	if (res) {
		printf("program compilation failed from %i, error= %i\n",posn,res);
		return -1;
	}
	return posn;
}
