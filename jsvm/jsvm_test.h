/* **********************************************************

    Jigsaw Virtual Machine (jsvm) Header

	Contains additional exports to allow low level unit testing

	@author: Howard Chivers
    @version: 1.0
    
	Copyright (c) 2015, Howard Chivers
	All rights reserved.
    
************************************************************ */

// expose internal core programs for testing

void resetThreads(Runtime * rtm);
void freeTemp(Runtime * rtm);
int getNextFromQueue(Runtime * rtm);
int allocateToTemp(Runtime * rtm);
void enqueueTemp(Runtime * rtm, uint8_t * const buffer);
bool checkAllocation(void **heap, int size, uint32_t *allocated, uint32_t *used, uint32_t requested, uint32_t increment, uint32_t maximum);

// common test utilities

int writeTestProgram(VirtualMachine * tvm, int instr[][4], int len);
int countChain(ThreadState * start);
int checkRe(VirtualMachine *tvm,Runtime * rtm,char * buffer,int start,int stop, int end,int count,int refResult[][3]);
