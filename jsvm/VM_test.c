/* **********************************************************

    Jigsaw Virtual Machine (tvms) Test Harness
    
    This runs as a console application
	as a preliminary test of the core c program tvms.c
	without its Python wrappers
    
    @author: Howard Chivers
    @version: 1.4
    
	Copyright (c) 2015, Howard Chivers
	All rights reserved.
    
************************************************************ */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "jsvm.h"
#include "jsvm_test.h"

#define test "all"

int main( int argc, const char* argv[] ) {
	bool pass,lpass;
	int res, j, k, l;
	uint32_t i, len;
	VirtualMachine tvm;

	//*********** Encoding (prog encode & decode)
	char buffer[1024];
	ByteCode instr;
	ByteCode prog[300];

	//************ Thread State Management
	Runtime rtm;
	ThreadState * pthread;

	//************test transition
	char trans_test_chars[5][2]= {"a","1","&","Z","]"};
	uint16_t trans_test_trans[5] = {0,1,1,0x10,CHARACTER_OK};
	char trans_test_set[32] = { 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80 };
	int trans_test_setref[5] = { 0, 58, 59, 60, 255 };

	//************newStart
	Start start;

    //**************findNextMatch()
								
	int check;
#define NO_OF_CHARACTERS 10
	char test_fm_trans[NO_OF_CHARACTERS][70] = {"a","b","n","o","xX","pq","0123456789","abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ","0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"};	
	int  test_fm_trans_len[NO_OF_CHARACTERS]= {1,1,1,1,2,2,10,52,62};

#define NUMBER_OF_PROGRAMS 11

int  test_fm_prog[NUMBER_OF_PROGRAMS][20][4] = {   //(instruction.index,address,last)
		   {{INSTR_MARK_START,0,0,0},				// RE0 = ab[xX]a[xX][pq](0,1)a(0,1)a(0,1)
			{INSTR_CHARACTER,0,0,0},
			{INSTR_CHARACTER,0,1,0},
			{INSTR_CHARACTER,0,4,0},
			{INSTR_CHARACTER,0,0,0},
			{INSTR_CHARACTER,0,4,0},
			{INSTR_SET_MARK,MAX_MARKS-1,99,0},      //check that set mark works
			{INSTR_NEW_THREAD,0,9,0},
			{INSTR_CHARACTER,0,5,0},
			{INSTR_NEW_THREAD,0,11,0},
			{INSTR_CHARACTER,0,0,0},  //10
			{INSTR_NEW_THREAD,0,14,0},
			{INSTR_CHARACTER,0,0,0},
			{INSTR_NEW_THREAD,0,14,1},			
			{INSTR_MARK_END,0,0,0},       //14
			{INSTR_PUBLISH,0,0,1}
		   },{                       
			{INSTR_MARK_START,0,0,0},						//RE1 = ano
			{INSTR_CHARACTER,0,0,0},
			{INSTR_CHARACTER,0,2,0},
			{INSTR_CHARACTER,0,3,0},
			{INSTR_MARK_END,0,0,0},        //20
			{INSTR_PUBLISH,0,0,1}
		   },{ 
			{INSTR_SET_CONTEXT,0,0,0},			   // RE2 = makes RE0 ab[]...  non-greedy
			{INSTR_NEW_THREAD,0,0,1}
		   },{
			{INSTR_MARK_START,0,0,0},						//RE3   = a(3,5)
			{INSTR_TEST + FLAG_TEST_GUARD, 0, 26, 1},
			{INSTR_SET_COUNT,0,3,0},				//a(3,0)
			{INSTR_CHARACTER + FLAG_SET_GUARD,0,0,0},
			{INSTR_TEST + FLAG_TEST_NOT + FLAG_TEST_COUNT, 0, 27,0},
			{INSTR_SET_COUNT,0,2,0},		//    a(0,2) 
			{INSTR_NEW_THREAD,0,34,0},     //30 
			{INSTR_CHARACTER,2,0,0},       
			{INSTR_TEST + FLAG_TEST_NOT + FLAG_TEST_COUNT, 0 , 30,0}, 
			{INSTR_NEW_THREAD,0,34,1},
			{INSTR_MARK_END,0,0,0},
			{INSTR_PUBLISH,0,0,0},
			{INSTR_TEST + FLAG_TEST_NOT, 0, 0,1}
		  },{ 
			{INSTR_SET_CONTEXT,0,0,0},				//RE4 = makes RE3 non-greedy
			{INSTR_TEST,0,24,1}
		  },{     
			{INSTR_SCAN,0,7,0},          //RE5   \b[alpha]*\b    (with zero width characters)
			{INSTR_SET_COUNT,0,100,0},  //40
			{INSTR_NEW_THREAD,0,46,0},   
			{INSTR_CHARACTER,0,7,0},    
			{INSTR_TEST + FLAG_TEST_NOT + FLAG_TEST_COUNT,0,41,0},  
			{INSTR_NEW_THREAD,0,45,1},
			{INSTR_CHARACTER | FLAG_ZERO_WIDTH,0,6,0},  
			{INSTR_MARK_END,0,0,0},
			{INSTR_PUBLISH,0,0,1}
		   },{
			{INSTR_CHARACTER,0,6,0},  					          //RE6   \b[alpha]*\b    (with mark byte reset)
			{INSTR_MARK_START,0,0,0},
			{INSTR_MARK_START,1,0,0},    //50
			{INSTR_CHARACTER,0,7,0},	  
			{INSTR_RESET_TO_MARK,1,0,0},        
			{INSTR_TEST,0,40,1}              
		   },{
			{INSTR_TEST + FLAG_TEST_FLAGS,VMFL_START,56,0},		// RE7   =  \b[alpha]\b  with end markers and previous 
			{INSTR_CHARACTER + FLAG_PREVIOUS,0,6,0},			//zero with boundary match /W-/w
			{INSTR_CHARACTER + FLAG_ZERO_WIDTH,0,7,0},
			{INSTR_MARK_START,0,0,0},							//loop init
			{INSTR_SET_COUNT,0,100,0},
			{INSTR_NEW_THREAD,0,63,0},							//optional loop on alpha
			{INSTR_CHARACTER,0,7,0},            //60
			{INSTR_TEST + FLAG_TEST_NOT + FLAG_TEST_COUNT,0,59,0},  
			{INSTR_NEW_THREAD,0,63,0},					
			{INSTR_TEST + FLAG_TEST_FLAGS,VMFL_END,65,0},		//end boundary
			{INSTR_CHARACTER+FLAG_ZERO_WIDTH,0,6,0},			//or not a word
			{INSTR_CHARACTER + FLAG_PREVIOUS,0,7,0},			//previous was a word 
			{INSTR_MARK_END,0,0,0},
			{INSTR_PUBLISH,0,0,1}
		   },{
			{INSTR_MARK_START,0,0,0},											// RE8 = alpha{,20}
			{INSTR_SET_COUNT,0,20,0},
			{INSTR_NEW_THREAD,0,73,0},	//70					//optional loop on alpha
			{INSTR_CHARACTER,0,7,0},            
			{INSTR_TEST + FLAG_TEST_NOT + FLAG_TEST_COUNT,0,70,0},
			{INSTR_NEW_THREAD,0,74,1},
			{INSTR_MARK_END,0,0,0},
			{INSTR_PUBLISH,0,0,1}
		   },{
			{INSTR_SCAN,0,0,0},									// guard testing, RE9 = a(a*|[a-z]*)x
			{INSTR_CHARACTER,0,0,0},					
			{INSTR_NEW_THREAD,0,83,0},		// alt
			{INSTR_TEST + FLAG_TEST_GUARD, 0, 80, 1},   //loop on [a]
			{INSTR_NEW_THREAD,0,87,0},		// 80
			{INSTR_CHARACTER + FLAG_SET_GUARD,0,0,0}, 
			{INSTR_TEST, 0, 80, 0},
			{INSTR_TEST + FLAG_TEST_GUARD, 1, 84, 1},   //loop on [a-z]
			{INSTR_NEW_THREAD,0,87,0},		
			{INSTR_CHARACTER + FLAG_SET_GUARD,0,7,0},     //85
			{INSTR_TEST, 0, 84, 0},
			{INSTR_CHARACTER,0,4,0},
			{INSTR_MARK_END,0,0,0},
			{INSTR_PUBLISH,0,0,1}
		   },{
			{INSTR_MARK_START,0,0,0}, //90      // RE10 backref test ([alpha]{3})\d{3}\1
			{INSTR_MARK_START,1,0,0},
			{INSTR_CHARACTER,0,7,0},
			{INSTR_CHARACTER,0,7,0},
			{INSTR_CHARACTER,0,7,0},
			{INSTR_MARK_END,1,0,0}, //95
			{INSTR_CHARACTER,0,6,0},
			{INSTR_CHARACTER,0,6,0},
			{INSTR_CHARACTER,0,6,0},
			{INSTR_TEST + FLAG_TEST_BACK, 1, 100, 1},
			{INSTR_MARK_END,0,0,0}, //100
			{INSTR_PUBLISH,0,0,1}}
			};

	int  test_fm_prog_len[NUMBER_OF_PROGRAMS] = {16,6,2,13,2,9,6,14,8,14,12};
	int test_fm_prog_map[NUMBER_OF_PROGRAMS];                   //maps progam numbers to actual location 
	int test_fm_start[3][2] = {{0,0},{0,1},{0,2}};				//(command, program index)
	char * test_fm_buffer0 = "absgaaanyanobabxaXatyvohutabXaxqzgyabxaxaaaaaohubsuond";
	char * test_fm_buffer1 = "anoyiuheohuieiehrvhuiepiuheruihreano";
	char * test_fm_buffer4 = "anoxanoxxanoxxxanoxxxxanoxxxxxanoxxxxxxanoxxxxxxanoxxx";
	char * test_fm_buffer5 = "12a34aa56aaa78aaaa90aaaaa01aaaaaa12aaaaaaa23aaaaaaaa11";
	char * test_fm_buffer6 = "the1quick69brown098fox778jumps0912over23the49lazy1dog1";
	char * test_fm_buffer7 = "the1quick69brown098fox778jumps0912over23the49lazy1dog";
	char * test_fm_buffer8 = "1234longmatchhere5678";
	char * test_fm_buffer9 = "aabc01ababx12baaab0abx123abcfe0aaax";
	char * test_fm_buffer10= "abc123ab&bcd123b&def123&abc123abcdef123def";
	char * test_fm_buffer11= "abc123abcdef123de";

#define CHECK_LENGTH 77
	int test_fm_ref[CHECK_LENGTH][3] =	{                               // (match_stat  match_end  match_last)
									{13,18,99},{26,31,99},{35,41,99},	//buffer 0 first start pattern
									{9,11,-1},							//buffer 0 2nd re		
									{13,17,99},{26,30,99},{35,39,99},	//buffer 0 1st re, short matching
									{13,18,99},{26,31,99},{35,40,99},	//repeat above, buffer restricted 13-41
									{13,17,99},{26,30,99},{35,39,99},
									{0,2,-1},{33,35,-1},				//buffer 1 - end check
									{4,6,-1},{15,17,-1},{48,50,-1},		//offset & stride anchors
									{9,11,-1},{14,17,-1},{20,24,-1},{27,31,-1},{35,39,-1},{44,48,-1},{49,51,-1},					  //loop count test
									{9,11,-1},{14,16,-1},{20,22,-1},{27,29,-1},{30,32,-1},{35,37,-1},{38,40,-1},{44,46,-1},{47,49,-1},//loop count non-greedy
									{0,2,-1},{4,8,-1},{11,15,-1},{19,21,-1},{25,29,-1},{34,37,-1},{40,42,-1},{45,48,-1},{50,52,-1},   //word boundaries zero width
									{4,8,-1},{11,15,-1},{19,21,-1},{25,29,-1},{34,37,-1},{40,42,-1},{45,48,-1},{50,52,-1},            //word boundaries reset mark
									{0,2,-1},{4,8,-1},{11,15,-1},{19,21,-1},{25,29,-1},{34,37,-1},{40,42,-1},{45,48,-1},{50,52,-1},   //word boundaries end marks
									{0,2,-1},																						  //fixed_anchor COMMAND
									{0,2,-1},																						  //noinc_anchor COMMAND
									{0,-1,-1},{1,0,-1},{2,1,-1},{3,2,-1},{4,16,-1},{17,16,-1},{18,17,-1},{19,18,-1},{20,19,-1},		  //zero width matches
									{6,10,-1},{19,21,-1},{31,34,-1},								  //loop guards
									{24,32,-1},{33,41,-1},											  //back references				
									{0,8,-1}};							
	//common startup
	memset(buffer,0xEE,sizeof(buffer));

	res = allocateMachine(&tvm);
	if (res) goto ErrorExit;  

	res = allocateRuntime(&rtm);
	if (res) goto ErrorExit;  
	pass = TRUE;


	//**********************************************************************************************
	//   Core subs
	//**********************************************************************************************
	
	lpass = TRUE;
	if (strcmp(test, "threads") || strcmp(test, "all")) {
		printf("Test: thread heap management\n");

		for (i=0;i<MAX_THREADHEAP_SIZE*2;i++) {
			res = allocateToTemp(&rtm);
			if (res == THREAD_HEAP_OVERFLOW) break;
			(*(rtm.temp)).p.byteIndex = i;
			enqueueTemp(&rtm, (uint8_t *) test_fm_buffer0);
		}
		if (rtm.queueLength>=MAX_THREADHEAP_SIZE+1) {
			printf("Failed to detect thread heap overflow\n");
			goto ErrorExit; 
		}
		if (rtm.queueLength!=MAX_THREADHEAP_SIZE) {
			printf("Thread records lost during allocate\n");
			lpass = FALSE;
			}

		resetThreads(&rtm);
		for (i=0;i<1000;i++) {
			allocateToTemp(&rtm);
			rtm.temp->p.byteIndex      = (i + 1)%17;
			rtm.temp->p.programCounter = (i + 2)%3;
			rtm.temp->p.counter[0]     = (i + 3)%11;
			rtm.temp->p.sequenceCount  = 0;
			rtm.temp->p.context        = 0;
			rtm.temp->p.startedMask    = 0;
			rtm.temp->p.markedMask     = 0;
			enqueueTemp(&rtm, (uint8_t *) test_fm_buffer0);
		}

		//check forward links
		j=1000000;  //previous byte size
		k=1000000;  //previous program counter size
		l=1000000;  //prevoius counter[0]
		for (i=0; i<rtm.queueLength; i++) {
			if (j<rtm.queue[i]->p.byteIndex) {
				printf("Incorrect descending byte order in queue %i then %i\n", j, rtm.queue[i]->p.byteIndex);
				lpass = FALSE;
				break;
			}
			if ((j==rtm.queue[i]->p.byteIndex) && (k<rtm.queue[i]->p.programCounter)) {
				printf("Incorrect descending source counter order in queue %i then %i\n", k, rtm.queue[i]->p.programCounter);
				lpass = FALSE;
				break;
			}
			j = rtm.queue[i]->p.byteIndex;
			k = rtm.queue[i]->p.programCounter;
			l = rtm.queue[i]->p.counter[0];
		}
		if (rtm.queueLength!= 561) {
			printf("Incorrect number of test entries in queue: %i\n",rtm.queueLength);
			lpass = FALSE;
		}

		if (!lpass) pass = FALSE;

		getNextFromQueue(&rtm);
		freeTemp(&rtm);         //just move one out indirectly
		getNextFromQueue(&rtm);
		*rtm.temp = *rtm.queue[rtm.queueLength-1];  //cloneThreadValue from queue into temp
		pthread  = rtm.temp;	//remember this clone
		enqueueTemp(&rtm, (uint8_t *) test_fm_buffer0);      //this should fail (is duplicate) and free temp
		getNextFromQueue(&rtm);
		freeTemp(&rtm);         //kill its parent
		getNextFromQueue(&rtm); //one more out of queue
		*rtm.temp = *pthread;   //cloneThreadValue(pthread,rtm.temp);
		enqueueTemp(&rtm, (uint8_t *) test_fm_buffer0);      //this should be unique and succeed
		if (rtm.queueLength!=558) {
			printf("Queue length incorrect after clones: %i not 558\n",rtm.queueLength);
			lpass = FALSE;
		}
		if (!lpass) pass = FALSE;

		//backref settings
		pthread = rtm.queue[rtm.queueLength-100];  //source for clone from somewhere in queue
		rtm.backrefMask = 2;
		allocateToTemp(&rtm);			//add reference
		*rtm.temp = *pthread;
		rtm.temp->groupMark[1].start = 13;
		rtm.temp->groupMark[1].end = 16;
		rtm.temp->p.markedMask = 2;
		enqueueTemp(&rtm, (uint8_t *) test_fm_buffer0);

		allocateToTemp(&rtm);			//length doesnt match
		*rtm.temp = *pthread;
		rtm.temp->groupMark[1].start = 13;
		rtm.temp->groupMark[1].end = 17;
		rtm.temp->p.markedMask = 2;
		enqueueTemp(&rtm, (uint8_t *) test_fm_buffer0);

		allocateToTemp(&rtm);			//content matches, no extra thread
		*rtm.temp = *pthread;
		rtm.temp->groupMark[1].start = 35;
		rtm.temp->groupMark[1].end = 38;
		rtm.temp->p.markedMask = 2;
		enqueueTemp(&rtm, (uint8_t *) test_fm_buffer0);

		allocateToTemp(&rtm);			//content match fails
		*rtm.temp = *pthread;
		rtm.temp->groupMark[1].start = 26;
		rtm.temp->groupMark[1].end = 29;
		rtm.temp->p.markedMask = 2;
		enqueueTemp(&rtm, (uint8_t *) test_fm_buffer0);

		allocateToTemp(&rtm);			//started is different
		*rtm.temp = *pthread;
		rtm.temp->groupMark[1].start = 26;
		rtm.temp->p.startedMask = 2;
		enqueueTemp(&rtm, (uint8_t *) test_fm_buffer0);

		allocateToTemp(&rtm);			//different start
		*rtm.temp = *pthread;
		rtm.temp->groupMark[1].start = 25;
		rtm.temp->p.startedMask = 2;
		enqueueTemp(&rtm, (uint8_t *) test_fm_buffer0);

		allocateToTemp(&rtm);			//same start
		*rtm.temp = *pthread;
		rtm.temp->groupMark[1].start = 26;
		rtm.temp->p.startedMask = 2;
		enqueueTemp(&rtm, (uint8_t *) test_fm_buffer0);

		if (rtm.queueLength!=563) {
			printf("Queue length incorrect after backreference sorting: %i not 563\n",rtm.queueLength);
			lpass = FALSE;
		}
		if (!lpass) pass = FALSE;

	}
	
	if (strcmp(test, "progalloc") || strcmp(test, "all")) {
		printf("Test: bytecode space allocation\n");

		if (tvm.allocatedInstructions != INIT_BYTECODE_SIZE) {
			printf("Initial bytecode allocation size incorrect\n");
			pass = FALSE;
		}

		// check initial
		len = INIT_BYTECODE_SIZE - 1;
		if (!checkAllocation((void **) &tvm.program, sizeof(ByteCode), &tvm.allocatedInstructions, &tvm.programLength, len, BYTECODE_SIZE_INC, MAX_BYTECODE_SIZE)) {
			printf("Initial bytecode allocation check failed\n");
			pass = FALSE;
		}

		// fill initial allocation
		for (i=0; i < INIT_BYTECODE_SIZE; i++) {
			tvm.program[i].address = (i + 7) % 353;
		}
		tvm.programLength = INIT_BYTECODE_SIZE;

		// request too much
		len = MAX_BYTECODE_SIZE + 1;
		if (checkAllocation((void **) &tvm.program, sizeof(ByteCode), &tvm.allocatedInstructions, &tvm.programLength, len, BYTECODE_SIZE_INC, MAX_BYTECODE_SIZE)) {
			printf("Excessive bytecode length request not detected\n");
			pass = FALSE;
		}

		// request more
		len = ((5 * BYTECODE_SIZE_INC)>>1);
		if (!checkAllocation((void **) &tvm.program, sizeof(ByteCode), &tvm.allocatedInstructions, &tvm.programLength, len, BYTECODE_SIZE_INC, MAX_BYTECODE_SIZE)) {
			printf("Excessive bytecode length request not detected\n");
			pass = FALSE;
		}
		len += INIT_BYTECODE_SIZE; 

		if (tvm.allocatedInstructions != INIT_BYTECODE_SIZE + (3 * BYTECODE_SIZE_INC)) {
			printf("Incorrect bytecode increment size\n");
			pass = FALSE;
		}

		if (pass) {
			// this might crash if above is incorrect
			for (i=INIT_BYTECODE_SIZE; i < len; i++) tvm.program[i].address = (i + 7) % 353;

			// check continuity
			for (i=0; i < len; i++) {
				if (tvm.program[i].address != ((i + 7) % 353)) {
					printf("Failed to maintain program continuity at %i during allocation.  %i\n",i,tvm.program[i].instruction);
					pass = FALSE;
					break;
				}
			}
		}
	}

	if (strcmp(test, "statealloc") || strcmp(test, "all")) {
		printf("Test: state space allocation\n");

		if (tvm.allocatedStates != INIT_STATE_SIZE) {
			printf("Initial state allocation size incorrect\n");
			pass = FALSE;
		}

		// check initial
		len = INIT_STATE_SIZE - 1;
		if (!checkAllocation((void **) &tvm.transition, 256 * sizeof(uint16_t), &tvm.allocatedStates, &tvm.stateLength, len, STATE_SIZE_INC, MAX_STATE_SIZE)) {
			printf("Initial state allocation check failed\n");
			pass = FALSE;
		}

		// fill initial allocation
		for (i=0; i < 256 * INIT_STATE_SIZE; i++) tvm.transition[i] = (i + 7) % 997;
		tvm.stateLength = INIT_STATE_SIZE;

		// request too much
		len = MAX_STATE_SIZE + 1;
		if (checkAllocation((void **) &tvm.transition, 256 * sizeof(uint16_t), &tvm.allocatedStates, &tvm.stateLength, len, STATE_SIZE_INC, MAX_STATE_SIZE)) {
			printf("Excessive state length request not detected\n");
			pass = FALSE;
		}

		// request more
		len =  (5 * STATE_SIZE_INC)/2;
		if (!checkAllocation((void **) &tvm.transition, 256 * sizeof(uint16_t), &tvm.allocatedStates, &tvm.stateLength, len, STATE_SIZE_INC, MAX_STATE_SIZE)) {
			printf("Excessive state length request not detected\n");
			pass = FALSE;
		}
		len += INIT_STATE_SIZE;

		if (tvm.allocatedStates != INIT_STATE_SIZE + (3 * STATE_SIZE_INC)) {
			printf("Incorrect state increment size\n");
			pass = FALSE;
		}

		if (pass) {
			// this might crash if above is incorrect
			for (i=INIT_STATE_SIZE; i < 256 * len; i++) tvm.transition[i] = (i + 7) % 997;

			// check continuity
			for (i=0; i < 256 * len; i++) {
				if (tvm.transition[i] != (i + 7) % 997) {
					printf("Failed to maintain program continuity during allocation.\n");
					pass = FALSE;
					break;
				}
			}
		}
	}

	freeMachine(&tvm);

	res = allocateMachine(&tvm);
	if (res) {
		printf("Re-allocate machine failed\n");
		goto ErrorExit; 
	}

	//*******************************************************************************************************
	//  loaders
	//*******************************************************************************************************
	
	if (strcmp(test, "newState") || strcmp(test, "program") || strcmp(test, "transition") || strcmp(test, "newStart") || strcmp(test, "all")) {
		//check newState detects out of order and fills with end
		printf("Test: newState()\n");
		memset(tvm.transition,0xFF,512 * sizeof(uint16_t));
		res = newStates(&tvm, 1, 2);
		if (res != INVALID_STATE_REQUESTED) {
			printf("newState() failed to detect out of order request (result = %i)\n",res);
			pass = FALSE;
		}

		res = newStates(&tvm, 0, 2);
		if (res) {
			printf("newState() failed at index %i, error = %i\n",i,res);
			pass = FALSE;
		}
		if (tvm.stateLength!=2) {
			printf("newState() failed to update stateLength at index %i\n",i);
			pass = FALSE;
		}

		for (i=0;i<512;i++) {
			if (tvm.transition[i]!=CHARACTER_FAIL) {
				printf("newState() failed to initialise all transitions to FAIL . Index = %i, value = %x\n",i);
				pass = FALSE;
				break;
			}
		}
		res = newStates(&tvm, 2, 32);   //for later tests
	}
	
	//**************************************************************************************************

	if (strcmp(test, "program") || strcmp(test, "newStart") || strcmp(test, "all")) {
		tvm.programLength = 0;
		printf("Test: newProgram()\n");
		instr.address = 66;
		instr.index   = 0;
		instr.instruction = INSTR_CHARACTER | FLAG_FINAL;
		res = newProgram(&tvm,&instr,1);
		if (res!=INVALID_TESTCHARACTER_ADDRESS) {
			printf("Failed to detect invalid character address. res = %i\n",res);
			pass = FALSE;
		}
		instr.index = MAX_COUNTERS;
		instr.instruction = INSTR_SET_COUNT | FLAG_FINAL;
		res = newProgram(&tvm,&instr,1);
		if (res!=INVALID_COUNTER_INDEX) {
			printf("Failed to detect invalid counter index. res = %i\n",res);
			pass = FALSE;
		}
		instr.address = 1;
		instr.index = MAX_MARKS;
		instr.instruction = INSTR_MARK_START| FLAG_FINAL;
		res = newProgram(&tvm,&instr,1);
		if (res!=INVALID_MARK_INDEX) {
			printf("Failed to detect invalid mark index. res = %i\n",res);
			pass = FALSE;
		}

		instr.instruction = INSTR_CHARACTER;
		instr.index = 0;
		for (i=0;i<10;i++) prog[i] = instr;

		res = newProgram(&tvm,prog,2);
		if (res!=END_PROGRAM_NOT_DETECTED) {
			printf("Failed to detect missing last entry flag. res = %i\n",res);
			pass = FALSE;
		}

		prog[2].instruction = INSTR_NEW_THREAD | FLAG_FINAL;
		prog[2].address     = 4;
		res = newProgram(&tvm,prog,3);
		if (res!=INVALID_PROGRAM_COUNTER) {
			printf("Failed to detect out of range bytecode address. res = %i\n",res);;
			pass = FALSE;
		}
		prog[1].instruction = INSTR_TEST | FLAG_FINAL;
		prog[1].address     = 6;
		res = newProgram(&tvm,prog,3);
		if (res!=INVALID_JUMP_ADDRESS) {
			printf("Failed to detect out of range conditional jump. res = %i\n",res);
			pass = FALSE;
		}	
	}

	//**********************************************************************************

	if (strcmp(test, "transition") || strcmp(test, "all")) {
		//check newTransition addressing and coding
		printf("Test: newTransition()\n");
		//write transitions
		for (i=0;i<5;i++) {
			res = newTransition(&tvm,i+7, (uint8_t *) &(trans_test_chars[i][0]),1,trans_test_trans[i]); 
			if (res) {
				printf("newTransition() failed at index %i, error = %i\n",i,res);
				pass = FALSE;
			}
		}
		res = newTransitionRange(&tvm,9,0xF7,0xFE,0x07); 
		if (res) {
			printf("newTransitionRange() failed at index %i, error = %i\n",i,res);
			pass = FALSE;
		}
		res = newTransitionSet(&tvm, 10, (uint8_t *) &trans_test_set, CHARACTER_OK);
		if (res) {
			printf("newTransitionSet() failed at index %i, error = %i\n", i, res);
			pass = FALSE;
		}
		//check
		for (i=0;i<5;i++) {
			j = (int)trans_test_chars[i][0];
			res = tvm.transition[(((i+7) * 256) + j)];
			if (res!=trans_test_trans[i]) {
				printf("newTransition() failed to write correct value at test %i\n",i);
				pass = FALSE;
			}
		}
		for (i=0xF7;i<0xFF;i++) {
			res = tvm.transition[((9 * 256) + i)];
			if (res!=0x07) {
				printf("newTransitionRange() failed to write correct value at test %i\n",i);
				pass = FALSE;
			}
		}
		for (i = 0; i < 5; i++) {
			res = tvm.transition[((10 * 256) + trans_test_setref[i])];
			if (res != CHARACTER_OK) {
				printf("newTransitionSet() failed to write correct value at test %i\n", i);
				pass = FALSE;
			}
		}
	}

	//**************************************************************************************

	if (strcmp(test, "newStart") || strcmp(test, "all")) {
		//check newStart addressing and coding, doesn't actually point at programs, just testing the compile sub
		printf("Test: newStart()\n");
		//should fail
		start.address = 0;
		start.command = RESERVED_MATCH;
		start.backrefMask = 0;
		res = newStart(&tvm,99,start);
		if (res != INVALID_START_REQUESTED) {
			printf("newStart() failed to detect out of order request (result = %i)\n",res); 
			pass = FALSE;
		}
		start.address = 99;
		start.command = 0;
		res = newStart(&tvm,0,start);
		if (res != START_REFERENCES_INVALID_PROGRAM) {
			printf("newStart() failed to detect invalid program address (result = %i)\n",res);
			pass = FALSE;
		}
		start.address = 1;
		start.command = 0;
		res = newStart(&tvm,0,start);
		if (res) {
			printf("newStart() 0 failed (result = %i)\n",res);
			pass = FALSE;
		}
		start.address = 0;
		start.command = SECTOR;
		res = newStart(&tvm,1,start);
		if (res) {
			printf("newStart() 1 failed (result = %i)\n",res);
			pass = FALSE;
		}
		if (tvm.startLength!=2) {
			printf("newStart() incorrect start length: %i\n",tvm.startLength);
			pass = FALSE;
		}
		
		if (tvm.startList[0].address!=(1) || tvm.startList[1].command!=(SECTOR)) {
			printf("newStart() incorrect expand values in list\n");
			pass = FALSE;
		}
	}

	//********************************************************************************
	//    VM tests
	//********************************************************************************

	if (strcmp(test, "findNextMatch") || strcmp(test, "all")) {
		//check matcher - does complete RE build
		printf("Test: findNextMatch()\n");
		
		//tables implement 0: ab[xX]a[xX][pq](0,1)a(0,1)a(0,1)       1: ano   2: as 0 but non-greedy   - seet tables for other REs

		//reset sanity
		tvm.stateLength			= 0;      
		tvm.programLength	    = 0;
		tvm.startLength			= 0;

		//Build character states:     a  b  n  o  xX  pq       test_fm_trans[6]
		res = newStates(&tvm, 0, NO_OF_CHARACTERS);
		if (res) {
			printf("newState() failed, result = %i\n",res);
			pass = FALSE;
		}
		for (i=0;i<NO_OF_CHARACTERS;i++) {
			res =  newTransition(&tvm,i,(uint8_t *) test_fm_trans[i],test_fm_trans_len[i],CHARACTER_OK);
			if (res) {
				printf("newTransition() index %i failed, result = %i\n",i,res);
				pass = FALSE;
			}
		}

		//Build program
		test_fm_prog_map[0] = 0;
		for (i=0;i<NUMBER_OF_PROGRAMS;i++) {
			res = writeTestProgram(&tvm,test_fm_prog[i],test_fm_prog_len[i]);
			 if (res==-1) {
				 pass = FALSE;
				 printf("Error was at program index %i\n",i);
				 break;
			 } else {
				 test_fm_prog_map[i] = res;
			 }
		}

		//Build starts
		for (i=0;i<3;i++) {
			start.command = test_fm_start[i][0];
			start.address = test_fm_prog_map[test_fm_start[i][1]];
			start.returnPairCount = 32;
			start.guardCount = 1;
			start.stride = 0;
			res = newStart(&tvm,i,start);
			if (res) {
				printf("newStart() %i failed (error = %i)\n",i,res);
				pass = FALSE;
			}
		}
		
		//now we can try to search
		check=0;
		printf("Match ********** forward match\n");
		check = checkRe(&tvm,&rtm,test_fm_buffer0,0,54,54,check,test_fm_ref);
		if (check == -1) {
			pass = FALSE;
		} 

		printf("Match ********** restrict buffer range\n");
		check = checkRe(&tvm,&rtm,test_fm_buffer0,13,40,41,check,test_fm_ref);
		if (check == -1) {
			pass = FALSE;
		} 
		
		printf("Match ********** buffer ends\n");
		check = checkRe(&tvm, &rtm,test_fm_buffer1,0,36,36,check,test_fm_ref);
		if (check == -1) {
			pass = FALSE;
		} 

		//
		//offset/stride counting
		//
		printf("Match ********** offset and stride\n");
		tvm.startLength = 0;
		for (i=0;i<3;i++) {
			start.command = test_fm_start[i][0]  | SECTOR;
			start.address = test_fm_prog_map[test_fm_start[i][1]];
			start.offset  = 4;
			start.stride  = 11;
			start.returnPairCount = 2;
			start.guardCount = 0;
			res = newStart(&tvm,i,start);
			if (res) {
				printf("newStart() %i failed (error = %i)\n",i,res);
				pass = FALSE;
			}
		}
		
		check = checkRe(&tvm,&rtm,test_fm_buffer4,0,54,54,check,test_fm_ref);
		if (check == -1) {
			pass = FALSE;
		} 

		//
		//loop counting
		//

		//setTrace(&tvm, TRACE_VERBOSE);
		printf("Match ********** loop counting greedy\n");
		tvm.startLength = 0;
		start.command = 0;
		start.address = test_fm_prog_map[3];
		start.returnPairCount = 2;
		start.guardCount = 1;
		start.stride = 0;
		res = newStart(&tvm,0,start);
		if (res) {
			printf("newStart() %i failed (error = %i)\n",i,res);
			pass = FALSE;
		}

		check = checkRe(&tvm,&rtm,test_fm_buffer5,0,54,54,check,test_fm_ref);
		if (check == -1) {
			pass = FALSE;
		} 

		printf("Match ********** loop counting non-greedy\n");
		tvm.startLength = 0;
		start.command = 0;
		start.address = test_fm_prog_map[4];
		start.returnPairCount = 2;
		start.guardCount = 1;
		res = newStart(&tvm,0,start);
		if (res) {
			printf("newStart() failed (error = %i)\n",res);
			pass = FALSE;
		}

		check = checkRe(&tvm,&rtm,test_fm_buffer5,0,54,54,check,test_fm_ref);
		if (check == -1) {
			pass = FALSE;
		} 
		
		//
		// boundary flags
		//

		printf("Match ********** zero width character/scan test \n");
		tvm.startLength = 0;
		start.command = 0;
		start.address = test_fm_prog_map[5];
		start.returnPairCount = 2;
		start.guardCount = 1;
		res = newStart(&tvm,0,start);
		if (res) {
			printf("newStart() failed (error = %i)\n",res);
			pass = FALSE;
		}

		check = checkRe(&tvm,&rtm,test_fm_buffer6,0,54,54,check,test_fm_ref);
		if (check == -1) {
			pass = FALSE;
		} 

		printf("Match ********** reset mark boundary test \n");
		tvm.startLength = 0;
		start.command = 0;
		start.address = test_fm_prog_map[6];
		start.returnPairCount = 2;
		start.guardCount = 1;
		//setTrace(&tvm, 2);

		res = newStart(&tvm,0,start);
		if (res) {
			printf("newStart() failed (error = %i)\n",res);
			pass = FALSE;
		}
		check = checkRe(&tvm,&rtm,test_fm_buffer6,0,54,54,check,test_fm_ref);
		if (check == -1) {
			pass = FALSE;
		} 
		//goto endWait;

		printf("Match ********** end flags boundary test \n");
		tvm.startLength = 0;
		start.command = 0;
		start.address = test_fm_prog_map[7];
		start.returnPairCount = 2;
		start.guardCount = 1;
		res = newStart(&tvm,0,start);
		if (res) {
			printf("newStart() failed (error = %i)\n",res);
			pass = FALSE;
		}

		check = checkRe(&tvm,&rtm,test_fm_buffer7,0,53,53,check,test_fm_ref);
		if (check == -1) {
			pass = FALSE;
		} 

		printf("Match ********** FIXED_ANCHOR command test \n");
		tvm.startLength = 0;
		start.command = FIXED_ANCHOR;
		start.address = test_fm_prog_map[7];
		start.returnPairCount = 2;
		start.guardCount = 1;
		res = newStart(&tvm,0,start);
		if (res) {
			printf("newStart() failed (error = %i)\n",res);
			pass = FALSE;
		}

		check = checkRe(&tvm,&rtm,test_fm_buffer7,0,53,53,check,test_fm_ref);
		if (check == -1) {
			pass = FALSE;
		} 

		printf("Match ********** NOINC_ANCHOR command test \n");
		tvm.startLength = 0;
		start.command = NOINC_ANCHOR;
		start.address = test_fm_prog_map[1];
		start.returnPairCount = 2;
		start.guardCount = 1;
		res = newStart(&tvm,0,start);
		if (res) {
			printf("newStart() failed (error = %i)\n",res);
			pass = FALSE;
		}

		check = checkRe(&tvm,&rtm,test_fm_buffer4,0,53,53,check,test_fm_ref);
		if (check == -1) {
			pass = FALSE;
		} 

		printf("Match ********** zero width matches test \n");
		tvm.startLength = 0;
		start.command = 0;
		start.address = test_fm_prog_map[8];
		start.returnPairCount = 2;
		start.guardCount = 1;
		res = newStart(&tvm,0,start);
		if (res) {
			printf("newStart() failed (error = %i)\n",res);
			pass = FALSE;
		}

		check = checkRe(&tvm,&rtm,test_fm_buffer8,0,21,21,check,test_fm_ref);
		if (check == -1) {
			pass = FALSE;
		} 

		printf("Match ********** guarded loops test \n");
		tvm.startLength = 0;
		start.command = 0; // ENABLE_PREFIX;
		start.address = test_fm_prog_map[9];
		start.returnPairCount = 2;
		start.guardCount = 2;
		res = newStart(&tvm,0,start);
		if (res) {
			printf("newStart() failed (error = %i)\n",res);
			pass = FALSE;
		}

		check = checkRe(&tvm,&rtm,test_fm_buffer9,0,35,35,check,test_fm_ref);
		if (check == -1) {
			pass = FALSE;
		} 

		printf("Match ********** backreference test \n");
		tvm.startLength = 0;
		start.command = 0; 
		start.address = test_fm_prog_map[10];
		start.returnPairCount = 2;
		start.guardCount = 0;
		res = newStart(&tvm,0,start);
		if (res) {
			printf("newStart() failed (error = %i)\n",res);
			pass = FALSE;
		}


		check = checkRe(&tvm,&rtm,test_fm_buffer10,0,42,42,check,test_fm_ref);
		if (check == -1) {
			pass = FALSE;
		} 
		
		check = checkRe(&tvm,&rtm,test_fm_buffer11,0,17,17,check,test_fm_ref);
		if (check == -1) {
			pass = FALSE;
		} 

		if (check!=CHECK_LENGTH) {
			printf("Test incomplete, count = %i, correct = 69\n",check);
			pass = FALSE;
		}

	}

	res = freeMachine(&tvm);
	if (res) goto ErrorExit;
	res = freeRuntime(&rtm);
	if (res) goto ErrorExit;

	if (pass) {
		printf("All Tests Pass\n");
	} else {
		printf("TEST ERRORS\n");
	}
endWait:
	getchar();
	exit(0);

ErrorExit:
	printf("Error Exit, code = %i\n",res);
	freeMachine(&tvm);
	freeRuntime(&rtm);
	getchar();
	exit(1);

}