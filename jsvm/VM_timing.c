/* **********************************************************

    Jigsaw Virtual Machine (jsvm) Tming Test

	Instruction speed tests for VM

	@author: Howard Chivers
    @version: 1.0
    
	Copyright (c) 2015, Howard Chivers
	All rights reserved.
    
************************************************************ */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "jsvm.h"
#include "jsvm_test.h"

#define TIMETEST "heap"

int buildStart(VirtualMachine * tvm, int addr, int cmd) {
	//Build simple start from given prog address
	Start start;
	int res;

	start.command = cmd;
	start.address = addr;
	start.returnPairCount = 32;
	start.guardCount = 1;
	start.stride = 0;
	res = newStart(tvm, 0, start);
	return res;
}

int main( int argc, const char* argv[] ) {

	int res;
	uint32_t i;
	VirtualMachine tvm;
	Runtime rtm;
	clock_t begin, end;
	double CPU_time;

	#define NUMBER_OF_PROGRAMS 3

	int  test_fm_prog[NUMBER_OF_PROGRAMS][20][4] = {   //(instruction.index,address,last)
		   {{INSTR_SET_COUNT,0,10000,0},			   // 10^8 test instructions	
			{INSTR_SET_COUNT,1,10000,0},
			{INSTR_TEST + FLAG_TEST_NOT + FLAG_TEST_COUNT,1,2,0},
		    {INSTR_TEST + FLAG_TEST_NOT + FLAG_TEST_COUNT,0,1,1}},
		   {{INSTR_SET_COUNT,0,10000,0},			   // 10^8 simple thread breaks	
			{INSTR_SET_COUNT,1,10000,0},
		    {INSTR_NEW_THREAD,0,7,1},
			{INSTR_TEST + FLAG_TEST_NOT + FLAG_TEST_COUNT,1,6,0},
		    {INSTR_TEST + FLAG_TEST_NOT + FLAG_TEST_COUNT,0,5,1}},
		   {{INSTR_SET_COUNT,0,1000,0},			   // 2000 fill and empty entries on 1000 high heap	
		    {INSTR_NEW_THREAD,0,12,0}, //10
			{INSTR_TEST + FLAG_TEST_NOT + FLAG_TEST_COUNT,0,10,1},
			{INSTR_SET_COUNT,0,1,0},	
			{INSTR_NEW_THREAD,0,14,1},
			{INSTR_TEST + FLAG_TEST_NOT + FLAG_TEST_COUNT,0,14,1}}
		   };

	int  test_fm_prog_len[NUMBER_OF_PROGRAMS] = {4, 5, 6};
	int test_fm_prog_map[NUMBER_OF_PROGRAMS];								//maps progam numbers to actual location 
	int test_fm_start[NUMBER_OF_PROGRAMS][2] = {{0,0},{0,1},{0,1}};			//(command, program index)

	char * test_fm_buffer0 = "absgaaanyanobabxaXatyvohutabXaxqzgyabxaxaaaaaohubsuond";

	//Build machines
	res = allocateMachine(&tvm);
	if (res) goto ErrorExit;  

	res = allocateRuntime(&rtm);
	if (res) goto ErrorExit;  

	//Build programs
	test_fm_prog_map[0] = 0;
	for (i=0;i<NUMBER_OF_PROGRAMS;i++) {
		res = writeTestProgram(&tvm, test_fm_prog[i], test_fm_prog_len[i]);
			if (res==-1) {;
				printf("Loader error at program index %i\n",i);
				break;
			} else {
				test_fm_prog_map[i] = res;
			}
	}

	rtm.bufferSpec.start = 0;
	rtm.bufferSpec.stop  = 1;
	rtm.bufferSpec.end   = 54;

	if (0 == strcmp(TIMETEST, "instruction")) {
		printf("Test: instruction timing: 10^8 test instructions\n");

		buildStart(&tvm, test_fm_prog_map[0], 0);
		initRunTime(&tvm, &rtm, 0);

		begin = clock();
		res = findNextMatch(&tvm, &rtm,(uint8_t *)test_fm_buffer0);
		end   = clock();
		if (res) goto ErrorExit;
	}

	if (0 == strcmp(TIMETEST, "thread")) {
		printf("Test: simple thread create/run timing: 10^8 cycles\n");

		res = buildStart(&tvm, test_fm_prog_map[1], 0);
		if (res) goto ErrorExit;
		res = initRunTime(&tvm, &rtm, 0);
		if (res) goto ErrorExit;

		begin = clock();
		res = findNextMatch(&tvm, &rtm,(uint8_t *)test_fm_buffer0);
		end   = clock();
		if (res) goto ErrorExit;
	}

	if (0 == strcmp(TIMETEST, "heap")) {
		// this deosn't test random insertions ...
		printf("Test: thread heap (1000 entries) fill/empty timing: 10^6 new threads\n");

		res = buildStart(&tvm, test_fm_prog_map[2], 0);
		if (res) goto ErrorExit;


		begin = clock();
		for (i=0; i<500; i++) {
			res = initRunTime(&tvm, &rtm, 0);
			if (res) goto ErrorExit;
			res = findNextMatch(&tvm, &rtm,(uint8_t *)test_fm_buffer0);
			if (res) goto ErrorExit;
		}
		end   = clock();
		
	}

	CPU_time = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("CPU Time = %0.5f\n", CPU_time);
	
	freeMachine(&tvm);
	freeRuntime(&rtm);
	getchar();
	exit(0);

	ErrorExit:
		printf("Error Exit, code = %i\n",res);
		freeMachine(&tvm);
		freeRuntime(&rtm);
		getchar();
		exit(1);
}